<?php
/**
 * @file
 * Install, update and uninstall hooks for the {{ profile }} subprofile.
 */

use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\field\Entity\FieldConfig;
use Drupal\lightning_core\ConfigHelper as Config;
use Drupal\user\Entity\User;
use Drupal\user\RoleInterface;

/**
 * Implements hook_install().
 */
function degov_install() {
  // Assign user 1 the "administrator" role.
  $user = \Drupal\user\Entity\User::load(1);
  $user->roles[] = 'administrator';
  $user->save();

  // Don't do anything else during config sync.
  if (\Drupal::isConfigSyncing()) {
    return;
  }

  // Restrict user registration to admin role creation.
  \Drupal::configFactory()
    ->getEditable('user.settings')
    ->set('register', USER_REGISTER_ADMINISTRATORS_ONLY)
    ->save(TRUE);

  // Allow authenticated users to use shortcuts.
  user_role_grant_permissions(RoleInterface::AUTHENTICATED_ID, ['access shortcuts']);

  // Set the default and admin theme.
  \Drupal::configFactory()
    ->getEditable('system.theme')
    ->set('default', 'bartik')
    ->set('admin', 'seven')
    ->save(TRUE);

  // Enable the admin theme.
  \Drupal::configFactory()
    ->getEditable('node.settings')
    ->set('use_admin_theme', TRUE)
    ->save(TRUE);
}

function degov_update_8440() {
  // Read the configuration.
  $module_data = \Drupal::config('core.extension')->get('module');

  // Remove lightning
  unset($module_data['lightning']);

  // Write the configuration.
  \Drupal::configFactory()->getEditable('core.extension')->set('module', $module_data)->save();
}

/**
 * Uninstalls the lightning API module. It will be removed in 2.1.x
 */
function degov_update_8441() {
  \Drupal::service('module_installer')->uninstall(['lightning_api']);
}
